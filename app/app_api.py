from flask import Flask, request, jsonify

## Aplicación
app = Flask(__name__)

## Máxima capacidad de archivo a recibir
app.config['MAX_CONTENT_LENGTH'] = 50 * 1024 * 1024

@app.route('/')
def hola():
	return '<h1>Hola! esta es una página de prueba</h1>'

@app.route('/tomonota')
def hola():
	return '<h1>Web: tomonota.net</h1>'
   
def pagina_no_encontrada(error):
    return '<h1>La página que intentas buscar no existe</h1>'

app.register_error_handler(404, pagina_no_encontrada)

# Recordar que esto solo se usa si accedes ejecutando python.
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=4450)